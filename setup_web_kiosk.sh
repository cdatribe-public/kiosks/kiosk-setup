#!/bin/bash

# Function to setup system configuration
# setup_system() {
#   # Configure keyboard, WiFi, autologin, etc.
# }

# Function to update and upgrade system
update_system() {
  echo "Updating system"
  sudo apt update
  sudo apt full-upgrade -y
}

# Function to install Argon Fan Hat software
install_fan_hat() {
  echo "Installing Argon Fan Hat"
  curl https://download.argon40.com/argonfanhat.sh | bash
}

# Function to install kiosk packages
install_kiosk_packages() {
  echo "Installing kiosk packages"
  sudo apt install -y --no-install-recommends \
  xserver-xorg-video-all \
  xserver-xorg-input-all \
  xserver-xorg-core \
  xinit \
  x11-xserver-utils \
  chromium-browser \
  unclutter \
  nginx \
  vim \
  git \
  git-lfs \
  ufw
}

# Function to create and configure .xinitrc
setup_xinitrc() {
echo "Creating .xinitrc file"
cat << 'EOF' > ~/.xinitrc
#!/usr/bin/env sh
xset -dpms
xset s off
xset s noblank

unclutter -idle 0 &

sed -i 's/"exited_cleanly":false/"exited_cleanly":true/' /home/pi/.config/chromium/Default/Preferences
sed -i 's/"exit_type":"Crashed"/"exit_type":"Normal"/' /home/pi/.config/chromium/Default/Preferences

/usr/bin/chromium-browser 127.0.0.1:80 \
--window-size=1920,1080 \
--window-position=0,0 \
--start-fullscreen \
--kiosk \
--incognito \
--noerrdialogs \
--disable-translate \
--no-first-run \
--fast \
--fast-start \
--disable-infobars \
--disable-features=TranslateUI \
--enable-features=OverlayScrollbars \
--disk-cache-dir=/dev/null \
--overscroll-history-navigation=0 \
--disable-pinch \
--autoplay-policy=no-user-gesture-required
EOF
}

# Function to edit .bash_profile
setup_bash_profile() {
  echo "Appending autostart startx to .bash_profile"
  STRING_TO_APPEND='if [ -z $DISPLAY ] && [ $(tty) = /dev/tty1 ];
  then
    startx
  fi'

  if ! grep -Fq "$STRING_TO_APPEND" ~/.bash_profile;
  then
      echo "$STRING_TO_APPEND" >> ~/.bash_profile
  fi
}

append_if_not_exist() {
  local line=$1
  local file=$2
  grep -qxF "$line" "$file" || echo "$line" | sudo tee -a "$file" > /dev/null
}

apply_optimizations() {
  echo "Modifying /boot/config.txt"
  FILE="/boot/config.txt"
  append_if_not_exist 'disable_splash=1' "$FILE"
  append_if_not_exist 'dtoverlay=disable-bt' "$FILE"
  append_if_not_exist 'boot_delay=0' "$FILE"
  # append_if_not_exist 'over_voltage=6' "$FILE"
  # append_if_not_exist 'arm_freq=2147' "$FILE"
  # append_if_not_exist 'gpu_freq=750' "$FILE"
}

update_cmdline() {
  echo "Modifying /boot/cmdline.txt"
  sudo cp /boot/cmdline.txt /boot/cmdline_backup.txt
  echo 'console=serial0,115200 console=tty1 root=PARTUUID=548f26fb-02 rootfstype=ext4 fsck.repair=yes rootwait cfg80211.ieee80211_regdom=US silent quiet splash loglevel=0 logo.nologo' | sudo tee /boot/cmdline.txt
}

install_and_configure_firewall() {
    echo "Installing and configuring firewall"
    sudo ufw default deny incoming
    sudo ufw default allow outgoing
    sudo ufw allow ssh
    sudo ufw allow 443
    sudo ufw --force enable
}

# Main execution flow
update_system
install_fan_hat
install_kiosk_packages
setup_xinitrc
setup_bash_profile
setup_splash_screen
apply_optimizations
update_cmdline
install_and_configure_firewall

# Reboot at the end
echo "Rebooting after kiosk setup script"
sudo reboot
